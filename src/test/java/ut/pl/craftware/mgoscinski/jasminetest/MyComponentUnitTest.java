package ut.pl.craftware.mgoscinski.jasminetest;

import org.junit.Test;
import pl.craftware.mgoscinski.jasminetest.MyPluginComponent;
import pl.craftware.mgoscinski.jasminetest.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}