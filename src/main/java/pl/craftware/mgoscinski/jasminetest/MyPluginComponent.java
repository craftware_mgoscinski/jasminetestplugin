package pl.craftware.mgoscinski.jasminetest;

public interface MyPluginComponent
{
    String getName();
}