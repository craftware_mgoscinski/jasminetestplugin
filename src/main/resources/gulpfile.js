var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var mainBowerFiles = require('main-bower-files');
var karma = require('gulp-karma');
var gulpFilter = require('gulp-filter');

var paths = {
  scripts: ['js/**/*.js', '!js/**/*.spec.js'],
  karmaFiles: ['build/js/vendor.js', 'js/**/*js']
};

var jsFilter = gulpFilter(['**/*.js'], {restore: true}),
    cssFilter = gulpFilter(['**/*.css'], {restore: true});

function throwOnError(error) {
    throw error;
}

gulp.task('test', function(coverage) {
  gulp.src(paths.karmaFiles)
      .pipe(karma({
        configFile: 'karma.conf.js',
        action: 'run'
      }))
      .on('error', throwOnError());
});

gulp.task('clean', function() {
  return del(['build']);
});

gulp.task('cleanScripts', function() {
    return del(['build/js/all.js']);
});

gulp.task('bower', ['clean'], function() {
  return gulp.src(mainBowerFiles({includeDev : true}), { base: 'bower_components' })
      .pipe(jsFilter)
      .pipe(concat('vendor.js'))
      .on('error', throwOnError)
      .pipe(gulp.dest('build/js'))
      .pipe(jsFilter.restore)
      .pipe(cssFilter)
      .pipe(concat('vendor.css'))
      .on('error', throwOnError)
      .pipe(gulp.dest('build/css'))
      .pipe(cssFilter.restore);
});

gulp.task('scripts', ['cleanScripts'], function() {
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
    .pipe(concat('all.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'));
});

gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
});

gulp.task('default', ['watch', 'bower', 'scripts']);