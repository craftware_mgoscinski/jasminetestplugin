angular.module('myApp').service('ProjectsService', ['ProjectsResource', function(ProjectsResource) {

    this.getAllProjectsPromise = function() {
        return ProjectsResource.query().$promise;
    };

}]);