describe("Dialog shower service", function() {

    var objectUnderTest;
    var AJS;
    var showSpy = jasmine.createSpy('show');

    beforeEach(module('myApp'));

    beforeEach(module(function($provide) {
        $provide.factory('AJS', function() {
            return {
                Dialog : function dialogConstructor() {
                    this.show = showSpy;
                }
            };
        });
    }));

    beforeEach(inject(function(_DialogShowerService_, _AJS_) {
        objectUnderTest = _DialogShowerService_;
        AJS = _AJS_;
    }));

    it("should create an AJS.Dialog object with a correct ID and call its show() method", function() {
        spyOn(AJS, 'Dialog').and.callThrough();
        objectUnderTest.showDialog('wololo');
        expect(AJS.Dialog).toHaveBeenCalledWith(jasmine.objectContaining({id : 'wololo'}));
        expect(showSpy).toHaveBeenCalled();
    });

});