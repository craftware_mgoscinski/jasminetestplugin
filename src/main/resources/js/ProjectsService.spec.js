describe("Projects service", function() {

    var objectUnderTest;
    var $httpBackend;
    beforeEach(module('myApp'));

    beforeEach(inject(function(_ProjectsService_, _$httpBackend_) {
        objectUnderTest = _ProjectsService_;
        $httpBackend = _$httpBackend_;
    }));

    it("should call the right endpoint on get all projects", function() {
        $httpBackend.expectGET('/jira/rest/api/2/project').respond(200, getSampleResponse());
        objectUnderTest.getAllProjectsPromise();
        $httpBackend.flush();
    });

    it("should pass a correct value wrapped in a promise", function() {
        var myResponse;
        $httpBackend.expectGET('/jira/rest/api/2/project').respond(200, getSampleResponse());

        objectUnderTest.getAllProjectsPromise().then(function(success) {
            myResponse = success;
        });
        $httpBackend.flush();

        expect(myResponse[0].id).toEqual('31337');
    });

    function getSampleResponse() {
        return [
            {
                "id": "31337",
                "key": "DEMO",
                "name": "Demonstration Project",
                "avatarUrls": {
                    "48x48": "http://google.com"
                }
            }
        ];
    }

});