angular.module('VisWrapper', []);

angular.module('VisWrapper').factory('Vis', [function() {

    return getGlobalVis();

    function getGlobalVis() {
        if(typeof(vis) === undefined || null === vis) {
            throw('Could not inject VisJS');
        }
        return vis;
    }

}]);