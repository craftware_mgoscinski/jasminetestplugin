var AJS = undefined;

describe("AJS Wrapper", function() {

    var objectUnderTest;
    var $injector;
    beforeEach(module('AtlassianJavaScript'));

    beforeEach(inject(function(_$injector_) {
        $injector = _$injector_;
    }));

    it("should throw an exception if global AJS is undefined", function() {
        AJS = undefined;
        expect(function() {
            objectUnderTest = $injector.get('AJS');
        }).toThrow();
    });

    it("should throw an exception if global AJS is null", function() {
        AJS = null;
        expect(function() {
            objectUnderTest = $injector.get('AJS');
        }).toThrow();
    });

    it("should return AJS from the global namespace", function() {
        AJS = function() {};
        AJS.version = '1337';
        objectUnderTest = $injector.get('AJS');
        expect(objectUnderTest).toBe(AJS);
    });

});