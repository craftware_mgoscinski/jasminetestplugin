angular.module('AtlassianJavaScript', []);

angular.module('AtlassianJavaScript').factory('AJS', [function() {

    return getGlobalAJS();

    function getGlobalAJS() {
        if(typeof(AJS) === undefined || null === AJS) {
            throw('Could not inject Atlassian AJS namespace');
        }
        return AJS;
    }

}]);