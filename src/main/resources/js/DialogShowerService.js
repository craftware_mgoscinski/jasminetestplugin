angular.module('myApp').service('DialogShowerService', ['AJS', function(AJS) {

    this.showDialog = function(customId) {
        var dialog = new AJS.Dialog({
            width: 640,
            height: 480,
            id: customId
        });

        dialog.show();
    };

}]);