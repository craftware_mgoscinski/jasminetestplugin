angular.module('myApp').controller('testController', [ '$scope', 'AJS', 'ProjectsService', 'WhateverService', 'DialogShowerService', controllerFunction ]);

function controllerFunction($scope, AJS, ProjectsService, WhateverService, DialogShowerService) {

    $scope.AJSversion = AJS.version;
    $scope.projects = {};
    $scope.whatever = WhateverService.whatever();

    ProjectsService.getAllProjectsPromise().then(function(success) {
        $scope.projects = success;
    });

    DialogShowerService.showDialog('eee');





}