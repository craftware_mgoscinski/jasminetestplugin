angular.module('myApp').factory('ProjectsResource', ['$resource', function($resource) {

    return $resource('/jira/rest/api/2/project');

}]);